Rails.application.routes.draw do
  get 'top/main'
  root 'top#main'
  resources :products
  resources :cartitems, only: [:create, :destroy]
  resources :carts, only: [:show]
end
