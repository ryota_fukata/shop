class Cartitem < ActiveRecord::Base
  belongs_to :carts
  has_one :products, dependent: :destroy
end
