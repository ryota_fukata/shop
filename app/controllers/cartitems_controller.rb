class CartitemsController < ApplicationController
  def create
    @product = Product.find(params[:product_id])
    @cartitem = Cartitem.new(qty: 1, product_id: @product.id, cart_id: params[:cart_id])
    @cartitem.save
    redirect_to top_main_path
  end

  def destroy
    Cartitem.find(params[:id]).delete
    redirect_to top_main_path
  end
end
