class ProductsController < ApplicationController
  def index
    @product = Product.all
  end

  def new
    @product = Product.new
    @cartitem = Cartitem.new
  end

  def create
    @product = Product.new(name: params[:product][:name], price: params[:product][:price])
    @product.save
    redirect_to top_main_path
  end

  def destroy
    if Cartitem.find_by(product_id: params[:id])
      Cartitem.find_by(product_id: params[:id]).delete
    end
    Product.find(params[:id]).delete
    redirect_to top_main_path
  end
end
